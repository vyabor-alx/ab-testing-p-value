## Instructions for running the app on a local machine

1. Install Docker and download the repo
2. Open a terminal in the directory of the Dockerfile included in this repo
3. Paste 'docker build -t pval'
4. After it is built, paste 'docker run -d --rm -p 8080:8080 pval'
5. Open Docker and open the link in browser